package main

import (
	"fmt"
	"strconv"

	"github.com/bwmarrin/discordgo"
	"github.com/replit/database-go"
)

// function to handle /set and its subcommands
func SetHandler(s *discordgo.Session, i *discordgo.InteractionCreate) {
	sc := i.ApplicationCommandData().Options[0].Name
	// subcommand switcher
	switch sc {
	// /set enabled
	case "enabled":
		if len(i.ApplicationCommandData().Options[0].Options) == 0 {
			cv, err := database.Get(fmt.Sprintf("%v-enabled", i.GuildID))
			var resp string
			if err != nil {
				resp = "nothing set"
			} else {
				resp = fmt.Sprintf("current value is %v", cv)
			}
			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: resp,
				},
			})

		} else {
			v := strconv.FormatBool(i.ApplicationCommandData().Options[0].Options[0].BoolValue())
			database.Set(fmt.Sprintf("%v-enabled", i.GuildID), v)
			gd, _ := s.State.Guild(i.GuildID)
			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: fmt.Sprintf("enabled for guild %v has been set to %v", gd.Name, v),
				},
			})
		}
	// /set channel
	case "channel":
		if len(i.ApplicationCommandData().Options[0].Options) == 0 {
			cv, err := database.Get(fmt.Sprintf("%v-channel", i.GuildID))
			var resp string
			if err != nil {
				resp = "nothing set"
			} else {
				resp = fmt.Sprintf("current value is <#%v>", cv)
			}
			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: resp,
				},
			})

		} else {
			v := i.ApplicationCommandData().Options[0].Options[0].ChannelValue(s)
			database.Set(fmt.Sprintf("%v-channel", i.GuildID), v.ID)
			dbID, err := database.Get(fmt.Sprintf("%v-webhook-id", i.GuildID))
			if err != nil {
				gd, _ := s.State.Guild(i.GuildID)
				s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
					Type: discordgo.InteractionResponseChannelMessageWithSource,
					Data: &discordgo.InteractionResponseData{
						Content: fmt.Sprintf("channel for guild %v has been set to <#%v>", gd.Name, v.ID),
					},
				})
			} else {
				wh, err := s.Webhook(dbID)
				if err != nil {
					s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
						Type: discordgo.InteractionResponseChannelMessageWithSource,
						Data: &discordgo.InteractionResponseData{
							Content: "couldn't get your webhook! (try setting it first)",
						},
					})
				} else {
					if wh.ChannelID != v.ID {
						co := fmt.Sprintf("WARNING!!! Your webhook channel is not the same as the channel that gets messages! The bot recieves messages in <#%v>, but the webhook will send messages to <#%v>. \n\nHowever, your channel was still set to\n `%v`!", v.ID, wh.ChannelID, v.ID)
						s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
							Type: discordgo.InteractionResponseChannelMessageWithSource,
							Data: &discordgo.InteractionResponseData{
								Content: co,
							},
						})
					} else {
						gd, _ := s.State.Guild(i.GuildID)
						s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
							Type: discordgo.InteractionResponseChannelMessageWithSource,
							Data: &discordgo.InteractionResponseData{
								Content: fmt.Sprintf("channel for guild %v has been set to <#%v>", gd.Name, v.ID),
							},
						})

					}
				}
			}
		}
	// /set webhook
	case "webhook":
		if len(i.ApplicationCommandData().Options[0].Options) == 0 {
			currentID, errID := database.Get(fmt.Sprintf("%v-webhook-id", i.GuildID))
			currentToken, errTK := database.Get(fmt.Sprintf("%v-webhook-token", i.GuildID))
			var resp string
			if errID != nil || errTK != nil {
				resp = "nothing set"
			} else {
				resp = fmt.Sprintf("current url is is https://discord.com/api/webhooks/%v/%v", currentID, currentToken)
			}
			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: resp,
					Flags:   discordgo.MessageFlagsEphemeral,
				},
			})

		} else {
			v := i.ApplicationCommandData().Options[0].Options[0].StringValue()
			mtch := webhook_regex.MatchString(v)
			var response string
			if !mtch {
				response = "⚠️ ERROR: invalid webhook URL"
			} else {
				hookData := webhook_regex.FindStringSubmatch(v)
				if len(hookData) != 3 {
					response = "⚠️ ERROR: invalid webhook URL"
				} else {
					hookID := hookData[1]
					hookToken := hookData[2]
					wh, err := s.Webhook(hookID)
					if err != nil {
						response = "Error getting webhook!"
					} else {
						ch, err := database.Get(fmt.Sprintf("%v-channel", i.GuildID))
						if err != nil {
							database.Set(fmt.Sprintf("%v-webhook-id", i.GuildID), hookID)
							database.Set(fmt.Sprintf("%v-webhook-token", i.GuildID), hookToken)
							response = fmt.Sprintf("set webhook to `%v`!", hookData[0])
						} else {
							database.Set(fmt.Sprintf("%v-webhook-id", i.GuildID), hookID)
							database.Set(fmt.Sprintf("%v-webhook-token", i.GuildID), hookToken)
							if wh.ChannelID != ch {
								response = fmt.Sprintf("WARNING!!! Your webhook channel is not the same as the channel that gets messages! The bot recieves messages in <#%v>, but the webhook will send messages to <#%v>. \n\nHowever, your webhook was still set to\n `%v`!", ch, wh.ChannelID, hookData[0])
							}
						}
					}
				}

			}
			s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
				Type: discordgo.InteractionResponseChannelMessageWithSource,
				Data: &discordgo.InteractionResponseData{
					Content: response,
					Flags:   discordgo.MessageFlagsEphemeral,
				},
			})
		}

	}
}

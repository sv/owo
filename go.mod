module gitlab.com/sv/owo

go 1.18

require (
	github.com/bwmarrin/discordgo v0.27.1
	github.com/mnlwldr/owo v0.1.0
)

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/joho/godotenv v1.5.1
	github.com/replit/database-go v0.0.0-20200814154522-f5159eae301b
	golang.org/x/crypto v0.7.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
)

# owo

a bot that does some owo stuff

## commands

`/owo message`: owoifies a message

`owoify (message context)`: owoifies the message the selected message

`/set enabled`: turns on auto owo (must have a channel set)

`/set channel`: sets a channel to automatically owoify messages in

`/set webhook`: set the webhook link that auto owo posts to

## usage
`cp .env.example .env`

add a discord token to TOKEN and a replit db url to REPLIT_DB_URL

`go get` `go mod tidy`

`go run main.go`

profit

package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"regexp"

	"github.com/bwmarrin/discordgo"
	"github.com/joho/godotenv"
	"github.com/mnlwldr/owo"
	"github.com/replit/database-go"
)

var (
	webhook_regex       = regexp.MustCompile(`(?m)^https:\/\/discord.com\/api\/webhooks\/(\d+)\/([a-zA-Z0-9_-]+)\/?$`)
	setPerm       int64 = discordgo.PermissionManageServer
	dmPerm              = false
	commands            = []*discordgo.ApplicationCommand{
		// slash commands
		{
			Name:        "owo",
			Description: "owoifies a message",
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionString,
					Name:        "text",
					Description: "what you want to owoify",
					Required:    true,
				},
			},
		},
		{
			Name:                     "set",
			Description:              "change settings for the bot",
			DefaultMemberPermissions: &setPerm,
			DMPermission:             &dmPerm,
			Options: []*discordgo.ApplicationCommandOption{
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "enabled",
					Description: "turn auto owo on/off",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Type:        discordgo.ApplicationCommandOptionBoolean,
							Name:        "enabled",
							Description: "on or off? (leave blank to see current value)",
							Required:    false,
						},
					},
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "channel",
					Description: "set channel to do owoing in",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Type:         discordgo.ApplicationCommandOptionChannel,
							Name:         "channel",
							Description:  "the channel to auto owoify messages in (leave blank to see current value)",
							Required:     false,
							ChannelTypes: []discordgo.ChannelType{discordgo.ChannelTypeGuildText},
						},
					},
				},
				{
					Type:        discordgo.ApplicationCommandOptionSubCommand,
					Name:        "webhook",
					Description: "set the webhook to post owo messages to",
					Options: []*discordgo.ApplicationCommandOption{
						{
							Type:        discordgo.ApplicationCommandOptionString,
							Name:        "webhook_url",
							Description: "the webhook url to post auto owo messages to (leave blank to see current)",
							Required:    false,
						},
					},
				},
			},
		},
		// context commands
		{
			Name: "owoify",
			Type: discordgo.MessageApplicationCommand,
		},
	}

	handlers = map[string]func(s *discordgo.Session, i *discordgo.InteractionCreate){
		"owo":    owoHandler,
		"owoify": owoifyHandler,
		"set":    SetHandler,
	}
)

func main() {
	godotenv.Load()
	dg, err := discordgo.New("Bot " + os.Getenv("TOKEN"))
	if err != nil {
		log.Fatalln(err)
	}

	dg.AddHandler(slashHandle)
	dg.AddHandler(messageHandle)

	dg.Identify.Intents = discordgo.IntentsAll

	if err = dg.Open(); err != nil {
		log.Fatalln("error opening session", err)
	}

	log.Println("adding commands...")
	registeredCommands := make([]*discordgo.ApplicationCommand, len(commands))
	for i, v := range commands {
		cmd, err := dg.ApplicationCommandCreate(dg.State.User.ID, "", v)
		if err != nil {
			log.Panicf("cannot create %v command: %v", v.Name, err)
		}
		registeredCommands[i] = cmd
	}

	defer dg.Close()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)
	log.Println("Press Ctrl+C to exit")
	<-stop

	log.Println("shutting down...")
}

func messageHandle(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author == s.State.User || m.Author == nil || m.Member == nil {
		return
	}
	en, err := database.Get(fmt.Sprintf("%v-enabled", m.GuildID))
	if en != "true" || err != nil {
		return
	}

	ch, err := database.Get(fmt.Sprintf("%v-channel", m.GuildID))
	if err != nil || m.ChannelID != ch {
		return
	}

	hookID, err := database.Get(fmt.Sprintf("%v-webhook-id", m.GuildID))
	if err != nil {
		return
	}
	hookToken, err := database.Get(fmt.Sprintf("%v-webhook-token", m.GuildID))
	if err != nil {
		return
	}

	_, err = s.WebhookExecute(hookID, hookToken, false, &discordgo.WebhookParams{
		Content:         owo.Translate(m.Content, false, true),
		Username:        m.Member.Nick,
		AllowedMentions: &discordgo.MessageAllowedMentions{},
		AvatarURL:       m.Author.AvatarURL("64"),
	})
	if err != nil {
		log.Printf("ERROR sending a webhook to guild %v: %v", m.GuildID, err.Error())
		ch, err := s.UserChannelCreate(m.Member.User.ID)
		if err != nil {
			log.Printf("ERROR couldn't make a DM with user %v: %v", m.Member.User.ID, err.Error())
		}
		s.ChannelMessageSend(ch.ID, fmt.Sprintf("couldn't send your message - here's the error (email this to suhas+errors@omg.lol if doesn't seem expected): %v", err.Error()))
	}
	err = s.ChannelMessageDelete(m.ChannelID, m.Message.ID)
	if err != nil {
		log.Printf("ERROR couldn't delete message from guild %v channel %v: %v", m.GuildID, m.ChannelID, err.Error())
	}

}

func slashHandle(s *discordgo.Session, i *discordgo.InteractionCreate) {
	if h, ok := handlers[i.ApplicationCommandData().Name]; ok {
		h(s, i)
	}
}

func owoHandler(s *discordgo.Session, i *discordgo.InteractionCreate) {
	opts := i.ApplicationCommandData().Options
	msg := opts[0]
	s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Content: owo.Translate(msg.StringValue(), false, true),
		},
	})
}

func owoifyHandler(s *discordgo.Session, i *discordgo.InteractionCreate) {
	s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{
			Flags:   discordgo.MessageFlagsEphemeral,
			Content: owo.Translate(i.ApplicationCommandData().Resolved.Messages[i.ApplicationCommandData().TargetID].Content, false, true),
		},
	})
}
